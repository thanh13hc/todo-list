import React, { Suspense } from "react";
import "./App.css";
import { Route, Routes } from "react-router-dom";
import ROUTES from "routes";
import GlobalProvider from "contexts/providers/GlobalProvider";
import ErrorBoundary from "components/ErrorBoundary";

function App() {
  return (
    <GlobalProvider>
      <ErrorBoundary>
        <Suspense fallback={<h1 className="text-center">Loading...</h1>}>
          <Routes>
            {ROUTES.map((item) => {
              return (
                <Route key={item.path} {...item} element={item.component} />
              );
            })}
          </Routes>
        </Suspense>
      </ErrorBoundary>
    </GlobalProvider>
  );
}

export default App;
