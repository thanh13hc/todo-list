import React, { createContext, useEffect, useReducer } from "react";
import globalReducer from "contexts/reducers/globalReducer";
export const GlobalContext = createContext({
  state: {},
  dispatch: ({ type = "" }: { type: string; payload?: any }) => {},
});

type Props = {
  children: React.ReactNode;
};

let initial = true;

const GlobalProvider: React.FC<Props> = ({ children }: Props) => {
  const [state, dispatch] = useReducer(globalReducer, {});

  useEffect(() => {
    if (initial) {
      initial = false;
      return;
    }
  }, []);

  return (
    <GlobalContext.Provider
      value={{
        state,
        dispatch,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};

export default GlobalProvider;
