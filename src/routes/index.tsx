import { Navigate } from "react-router-dom";
import { ROUTES_URL } from "common/enums";
import React from "react";

const ToDoListPage = React.lazy(() => import("pages/to-do/index"));

const ROUTES = [
  {
    path: ROUTES_URL.DEFAULT,
    component: <Navigate to={ROUTES_URL.TO_DO_LIST} />,
  },
  { path: ROUTES_URL.TO_DO_LIST, component: <ToDoListPage /> },
];

export default ROUTES;
