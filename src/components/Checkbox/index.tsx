import React from "react";
import styles from "components/Checkbox/index.module.css";

type Props = {
  name: string;
  label?: string;
  value: boolean;
  onChange: (value: boolean) => void;
};

const Checkbox: React.FC<Props> = (props: Props) => {
  const { name, label, value, onChange } = props;
  return (
    <div>
      <label htmlFor={name}>{label}</label>
      <input
        id={name}
        type="checkbox"
        className={styles.input}
        checked={value}
        onChange={(e) => {
          onChange(e.target.checked);
        }}
      />
    </div>
  );
};

export default Checkbox;
