import React from "react";
import styles from "components/Button/index.module.css";
import { ButtonColor } from "common/enums/types";

type Props = {
  label: string;
  color: ButtonColor;
  block?: boolean;
  onClick: () => void;
};

const Button: React.FC<Props> = (props: Props) => {
  const { label, color, block = false } = props;

  const classes = `${styles.button} ${styles[color]} ${
    block ? styles.block : ""
  }`;

  return (
    <button className={classes} onClick={props.onClick}>
      {label}
    </button>
  );
};

export default Button;
