import React from "react";
import styles from "components/InputText/index.module.css";

type Props = {
  name: string;
  label?: string;
  value: string;
  onChange: (value: string) => any;
  placeholder: string;
  debounce?: number;
  required?: boolean;
};

const InputText: React.FC<Props> = (props: Props) => {
  const {
    name,
    label,
    value,
    onChange,
    placeholder,
    required = false,
  } = props;

  return (
    <div className="form-group">
      <label htmlFor={name}>{label}</label>
      <input
        type="text"
        id={name}
        className={`${styles.input} form-control`}
        value={value}
        onChange={(e) => {
          const value = e.target.value;
          onChange(value);
        }}
        required={required}
        placeholder={placeholder}
      />
    </div>
  );
};

export default InputText;
