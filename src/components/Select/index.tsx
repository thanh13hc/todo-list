import React from "react";
import { CommonOption } from "common/enums/types";
import styles from "components/Select/index.module.css";
import { generateId } from "common/helper";

type Props = {
  name: string;
  label: string;
  options: CommonOption[];
  value: string;
  onChange: (value: string) => void;
};

const Select: React.FC<Props> = (props: Props) => {
  const { name, label, options, value, onChange } = props;
  return (
    <div className="form-group">
      <label htmlFor={name}>{label}</label>
      <select
        name={name}
        id={name}
        className={`${styles.select} form-control`}
        onChange={(e) => onChange(e.target.value)}
        value={value}
      >
        {options.map((item) => (
          <option
            value={item.value}
            key={generateId("select-option", item.value)}
          >
            {item.label}
          </option>
        ))}
      </select>
    </div>
  );
};

export default Select;
