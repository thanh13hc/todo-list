import React, { useRef } from "react";
import styles from "components/InputDate/index.module.css";
import moment from "moment";
import { DATE_FORMAT } from "common/enums";

type Props = {
  name: string;
  label: string;
  value: string;
  onChange: (value: string) => void;
  min?: string;
  max?: string;
};

const InputDate: React.FC<Props> = (props: Props) => {
  const { name, label, value, onChange } = props;
  const inputRef = useRef<any>(null);

  return (
    <div className="form-group">
      <label htmlFor={name}>{label}</label>
      <div className={styles["input-wrapper"]}>
        <input
          ref={inputRef}
          id={name}
          type="date"
          formatted-date={moment(value, DATE_FORMAT.dashWithYearFirst).format(
            DATE_FORMAT.dayWithMonthNameFirst
          )}
          className={`${styles.input} form-control`}
          min={props.min ?? moment().format(DATE_FORMAT.dashWithYearFirst)}
          max={props.max}
          value={value}
          onChange={(e) => onChange(e.target.value)}
        />
        <i
          className="fa-solid fa-calendar-days"
          onClick={() => {
            inputRef.current!.showPicker();
          }}
        ></i>
      </div>
    </div>
  );
};

export default InputDate;
