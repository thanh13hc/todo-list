import React from "react";
import styles from "components/TextArea/index.module.css";

type Props = {
  name: string;
  label: string;
  rows?: number;
  value: string;
  onChange: (value: string) => void;
};

const TextArea: React.FC<Props> = (props: Props) => {
  const { name, label, rows = 10, value, onChange } = props;
  return (
    <div className="form-group">
      <label htmlFor={name}>{label}</label>
      <textarea
        rows={rows}
        id={name}
        className={`${styles["text-area"]} form-control`}
        value={value}
        onChange={(e) => onChange(e.target.value)}
      />
    </div>
  );
};

export default TextArea;
