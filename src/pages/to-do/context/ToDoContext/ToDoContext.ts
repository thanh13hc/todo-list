/* eslint-disable react-hooks/rules-of-hooks */
import { initialToDoState } from "pages/to-do/common/constans";
import { createContext, useContext } from "react";

const ToDoContext = createContext({
  state: initialToDoState,
  dispatch: ({ type = "" }: { type: string; payload?: any }) => {},
});

export const useToDoContext = () => useContext(ToDoContext);

export default ToDoContext;
