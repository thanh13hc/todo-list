import { DATE_FORMAT } from "common/enums";
import { ToDoModel } from "common/enums/types";
import { sortArray } from "common/helper";
import { cloneDeep } from "lodash";
import moment from "moment";
import { ToDoState } from "pages/to-do/common/constans";

export const getToDos = (state: ToDoState): ToDoModel[] => {
  if (state.todos.length === 0) return [];

  const today = moment().format(DATE_FORMAT.dashWithYearFirst);
  let indexRecordNearToday = -1;
  let hasIncommingToDos = false;
  let hasPassedToDos = false;
  let todos = sortArray<ToDoModel>({
    list: cloneDeep(state.todos),
    key: "dueDate",
    type: "asc",
  });

  todos.forEach((td, arrIndex) => {
    const todoDueDate = moment(td.dueDate).format(
      DATE_FORMAT.dashWithYearFirst
    );
    if (todoDueDate >= today) {
      hasIncommingToDos = true;

      if (indexRecordNearToday === -1) indexRecordNearToday = arrIndex;
    } else {
      hasPassedToDos = true;
    }
  });

  if (!hasIncommingToDos || !hasPassedToDos || indexRecordNearToday === -1) {
    return todos;
  }

  let passedToDos = todos.slice(0, indexRecordNearToday);
  let incomingToDos = todos.slice(indexRecordNearToday, todos.length);

  todos = [...incomingToDos, ...passedToDos];

  return todos;
};
