import { ToDoModel } from "common/enums/types";
import { TO_DO_ACTION_TYPE } from "pages/to-do/common/constans";

export const createToDo = (payload: ToDoModel) => {
  return { type: TO_DO_ACTION_TYPE.CREATE, payload };
};

export const updateToDo = (payload: ToDoModel) => {
  return { type: TO_DO_ACTION_TYPE.UPDATE, payload };
};

export const removeToDo = (payload: string[]) => {
  return { type: TO_DO_ACTION_TYPE.REMOVE, payload };
};

export const loadToDo = (payload: ToDoModel[]) => {
  return { type: TO_DO_ACTION_TYPE.LOAD, payload };
};
