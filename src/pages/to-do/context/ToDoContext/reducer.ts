import { cloneDeep } from "lodash";
import {
  initialToDoState,
  TO_DO_ACTION_TYPE,
} from "pages/to-do/common/constans";

export const toDoReducer = (
  state = initialToDoState,
  { type, payload }: { type: string; payload?: any }
) => {
  switch (type) {
    case TO_DO_ACTION_TYPE.CREATE:
      return { ...state, todos: [...state.todos, payload] };

    case TO_DO_ACTION_TYPE.UPDATE:
      const index = state.todos.findIndex((td) => td.id === payload.id);
      const newList = cloneDeep(state.todos);
      newList.splice(index, 1, payload);
      return { ...state, todos: newList };

    case TO_DO_ACTION_TYPE.REMOVE:
      return {
        ...state,
        todos: [...state.todos].filter((item) => !payload.includes(item.id)),
      };

    case TO_DO_ACTION_TYPE.LOAD:
      return { ...state, todos: [...payload] };
  }

  return state;
};
