import { generateId } from "common/helper";
import InputText from "components/InputText";
import React, { useState } from "react";
import ReactDOM from "react-dom";
import { onRemoveToDo } from "../common/helper";
import ModalAction from "../components/ModalAction";
import ToDoCard from "../components/ToDoCard";
import { getToDos } from "../context/ToDoContext/selector";
import { useToDoContext } from "../context/ToDoContext/ToDoContext";

type Props = {};

const ToDoListContainer: React.FC<Props> = (props: Props) => {
  const { state, dispatch } = useToDoContext();
  let toDos = getToDos(state);
  const [filter, setFilter] = useState("");
  const [selectedValues, setSelectedValues] = useState<string[]>([]);
  const [viewDetail, setViewDetail] = useState<string>("");
  const onRemove = onRemoveToDo.bind(null, dispatch, selectedValues);

  toDos = toDos.filter((item) => {
    return filter ? item.title.includes(filter) : true;
  });

  return (
    <>
      <h1 className="text-center">To Do List</h1>

      <InputText
        value={filter}
        onChange={setFilter}
        name="search"
        placeholder="Search ..."
      />

      {toDos.length > 0 ? (
        <>
          {toDos.map((todo) => {
            const isSelected = selectedValues.includes(todo.id);
            const onCheck = () =>
              setSelectedValues((preState: string[]) =>
                !isSelected
                  ? [...preState, todo.id]
                  : [...preState].filter((item) => item !== todo.id)
              );
            const onClickViewDetail = () =>
              setViewDetail((preState) =>
                preState === todo.id ? "" : todo.id
              );

            return (
              <ToDoCard
                todo={{ ...todo }}
                key={generateId("to-do-card", todo.id)}
                isSelected={isSelected}
                onCheck={onCheck}
                showDetail={viewDetail === todo.id}
                onClickViewDetail={onClickViewDetail}
              />
            );
          })}
        </>
      ) : (
        <h1 className="empty">No record found</h1>
      )}

      {selectedValues.length > 0 &&
        ReactDOM.createPortal(
          <ModalAction
            onRemoveClick={() => {
              onRemove();
              setSelectedValues([]);
            }}
          />,
          document.getElementById("portal-root")!
        )}
    </>
  );
};

export default ToDoListContainer;
