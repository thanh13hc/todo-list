import { InitialToDo } from "common/enums";
import React from "react";
import { onCreateToDo } from "../common/helper";
import FormContainer from "../components/FormContainer";
import { useToDoContext } from "../context/ToDoContext/ToDoContext";

const AddToDoContainer: React.FC = () => {
  const { dispatch } = useToDoContext();

  return (
    <>
      <h1 className="text-center">New Task</h1>
      <FormContainer
        mode="create"
        initialValues={{ ...InitialToDo }}
        onSubmit={onCreateToDo.bind(null, dispatch)}
      />
    </>
  );
};

export default AddToDoContainer;
