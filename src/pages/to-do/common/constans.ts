import { ToDoModel } from "common/enums/types";

export const PIORYTY_OPTIONS = [
  { label: "Low", value: "low" },
  { label: "Normal", value: "normal" },
  { label: "High", value: "high" },
];

export type ToDoState = {
  todos: ToDoModel[];
};

export const initialToDoState: ToDoState = {
  todos: [],
};

export enum TO_DO_ACTION_TYPE {
  CREATE = "CREATE",
  LOAD = "LOAD",
  UPDATE = "UPDATE",
  REMOVE = "REMOVE",
}
