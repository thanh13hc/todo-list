import { ToDoModel } from "common/enums/types";
import { generateId } from "common/helper";
import {
  createToDo,
  removeToDo,
  updateToDo,
} from "../context/ToDoContext/actionCreator";

export const onCreateToDo = (
  dispatch: (params: any) => void,
  formValues: ToDoModel
) => {
  dispatch(createToDo({ ...formValues, id: generateId("to-do-item") }));
};

export const onUpdateToDo = (
  dispatch: (params: any) => void,
  formValues: ToDoModel
) => {
  dispatch(updateToDo({ ...formValues }));
};

export const onRemoveToDo = (
  dispatch: (params: any) => void,
  ids: string[]
) => {
  dispatch(removeToDo(ids));
};
