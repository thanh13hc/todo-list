import { ToDoModel } from "common/enums/types";
import React from "react";
import FormContainer from "./FormContainer";
import ToDoCardHeader from "./ToDoCardHeader";
import styles from "pages/to-do/common/css/toDoCard.module.css";
import { useToDoContext } from "../context/ToDoContext/ToDoContext";
import { onUpdateToDo, onRemoveToDo } from "../common/helper";

type Props = {
  todo: ToDoModel;
  isSelected: boolean;
  onCheck: any;
  showDetail: boolean;
  onClickViewDetail: () => void;
};

const ToDoCard: React.FC<Props> = (props: Props) => {
  const { todo, isSelected, onCheck, showDetail, onClickViewDetail } = props;
  const { dispatch } = useToDoContext();

  return (
    <div className={`${styles["to-do-card"]}`}>
      <ToDoCardHeader
        isSelected={isSelected}
        onCheck={onCheck}
        setShowDetail={() => onClickViewDetail()}
        title={todo.title}
        onRemove={onRemoveToDo.bind(null, dispatch, [todo.id])}
      />
      {showDetail && (
        <div className={`${styles["card-body"]}`}>
          <FormContainer
            mode="update"
            initialValues={{ ...todo }}
            onSubmit={onUpdateToDo.bind(null, dispatch)}
          />
        </div>
      )}
    </div>
  );
};

export default ToDoCard;
