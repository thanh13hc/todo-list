import React from "react";
import Button from "components/Button";
import styles from "pages/to-do/common/css/modalAction.module.css";

type Props = {
  onRemoveClick: () => void;
};

const ModalAction: React.FC<Props> = (props: Props) => {
  return (
    <div className={styles.container}>
      <p>Bulk Action:</p>
      <div className={styles["action-buttons"]}>
        <Button label="Done" color="primary" onClick={() => undefined} />
        <Button label="Remove" color="danger" onClick={props.onRemoveClick} />
      </div>
    </div>
  );
};

export default ModalAction;
