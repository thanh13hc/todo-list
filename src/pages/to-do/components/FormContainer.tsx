import React, { useEffect, useState } from "react";
import Button from "components/Button";
import InputDate from "components/InputDate";
import InputText from "components/InputText";
import Select from "components/Select";
import TextArea from "components/TextArea";
import { PIORYTY_OPTIONS } from "../common/constans";
import { FormMode, ToDoModel } from "common/enums/types";
import { InitialToDo } from "common/enums";

type Props = {
  mode: FormMode;
  initialValues: ToDoModel;
  onSubmit: any;
};

const FormContainer: React.FC<Props> = (props: Props) => {
  const [formValues, setFormValues] = useState(props.initialValues);
  useEffect(() => {
    setFormValues(props.initialValues);
  }, [props.initialValues]);

  return (
    <>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          props.onSubmit({ ...formValues });
          setFormValues({ ...InitialToDo });
        }}
      >
        <InputText
          placeholder="Add new task ..."
          value={formValues.title}
          onChange={(value) =>
            setFormValues((preState) => ({ ...preState, title: value }))
          }
          name={"task-title"}
          required
        />

        <TextArea
          name="description"
          label="Description"
          value={formValues.description}
          onChange={(value) =>
            setFormValues((preState) => ({ ...preState, description: value }))
          }
        />
        <div className="d-grid-2 grid-gap-2">
          <InputDate
            name="due-date"
            label="Due Date"
            value={formValues.dueDate}
            onChange={(value) => {
              setFormValues((preState) => ({ ...preState, dueDate: value }));
            }}
          />
          <Select
            name="Piority"
            label="Piority"
            options={PIORYTY_OPTIONS}
            value={formValues.priority}
            onChange={(value) =>
              setFormValues((preState) => ({ ...preState, priority: value }))
            }
          />
        </div>

        <Button
          label={props.mode === "create" ? "Add" : "Update"}
          color="success"
          block
          onClick={() => undefined}
        />
      </form>
    </>
  );
};

export default FormContainer;
