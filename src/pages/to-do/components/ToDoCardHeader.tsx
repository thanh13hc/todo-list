import Button from "components/Button";
import React from "react";
import styles from "pages/to-do/common/css/toDoCard.module.css";
import Checkbox from "components/Checkbox";

type Props = {
  title: string;
  isSelected: boolean;
  onCheck: any;
  setShowDetail: () => void;
  onRemove: () => void;
};

const ToDoCardHeader: React.FC<Props> = (props: Props) => {
  return (
    <div
      className={`d-flex justify-space-between align-center ${styles["card-header"]}`}
    >
      <div className="d-flex align-center">
        <Checkbox
          name="tick"
          value={props.isSelected}
          onChange={() => props.onCheck()}
        />
        <span>{props.title}</span>
      </div>

      <div className={styles["action-header"]}>
        <Button
          label="Detail"
          color="info"
          onClick={() => props.setShowDetail()}
        />
        <Button label="Remove" color="danger" onClick={props.onRemove} />
      </div>
    </div>
  );
};

export default ToDoCardHeader;
