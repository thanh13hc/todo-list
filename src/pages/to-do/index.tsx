import React, { useEffect, useReducer } from "react";
import AddToDoContainer from "./container/AddToDoContainer";
import ToDoListContainer from "./container/ToDoListContainer";
import styles from "pages/to-do/common/css/index.module.css";
import { ToDoModel } from "common/enums/types";
import ToDoContext from "./context/ToDoContext/ToDoContext";
import { toDoReducer } from "./context/ToDoContext/reducer";
import { initialToDoState } from "./common/constans";
import { loadToDo } from "./context/ToDoContext/actionCreator";

let initialPage = true;

const ToDoListPage: React.FC = () => {
  const [state, dispatch] = useReducer(toDoReducer, initialToDoState);

  useEffect(() => {
    const localTodos = localStorage.getItem("to-do-list");

    if (localTodos) {
      const todos: ToDoModel[] = JSON.parse(localTodos);

      dispatch(loadToDo(todos as ToDoModel[]));
    }
  }, []);

  useEffect(() => {
    if (!initialPage) {
      localStorage.setItem("to-do-list", JSON.stringify(state.todos));
      return;
    }

    initialPage = false;
  }, [state.todos]);

  return (
    <ToDoContext.Provider value={{ state, dispatch }}>
      <div className={styles["to-do-list"]}>
        <div className={`${styles.container} ${styles["add-container"]}`}>
          <AddToDoContainer />
        </div>
        <div className={`${styles.container} ${styles["list-container"]}`}>
          <ToDoListContainer />
        </div>
      </div>
    </ToDoContext.Provider>
  );
};

export default ToDoListPage;
