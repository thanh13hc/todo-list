import { get } from "lodash";

export const debounce = <F extends (...args: any[]) => any>(
  func: F,
  waitFor: number
) => {
  let timeout: ReturnType<typeof setTimeout>;

  const debounced = (...args: any[]) => {
    clearTimeout(timeout);
    timeout = setTimeout(() => func(...args), waitFor);
  };

  return debounced;
};

export const generateId = (name: string = "", key: string = "") => {
  return `${name}_${key}_${new Date().getTime()}_${Math.round(
    Math.random() * 1000
  )}`;
};

export const sortArray = <T>({
  list = [],
  key = "",
  type = "asc",
}: {
  list: T[];
  key: string;
  type: "asc" | "desc";
}): T[] => {
  return list.sort((firstItem, secondItem) => {
    const firstItemVal: any = key ? get(firstItem, key) : firstItem;
    const secondItemVal: any = key ? get(secondItem, key) : secondItem;

    if (!firstItemVal || !secondItemVal) {
      if (!firstItemVal) return type === "asc" ? -1 : 1;

      if (!secondItemVal) return type === "asc" ? 1 : -1;
    }

    switch (typeof firstItemVal) {
      case "number":
        return type === "asc"
          ? firstItemVal - secondItemVal
          : secondItemVal - firstItemVal;
      case "string":
        return type === "asc"
          ? firstItemVal.localeCompare(secondItemVal)
          : secondItemVal.localeCompare(firstItemVal);
      default:
        return 0;
    }
  });
};
