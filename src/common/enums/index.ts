import moment from "moment";
import { PIORYTY_OPTIONS } from "pages/to-do/common/constans";
import { ToDoModel } from "./types";

export enum ROUTES_URL {
  DEFAULT = "/",
  TO_DO_LIST = "/to-do-list",
}

export enum DATE_FORMAT {
  dashWithYearFirst = "YYYY-MM-DD",
  dayWithMonthNameFirst = "DD MMMM YYYY",
}

export enum GLOBAL_ACTIONS_TYPE {}

export const InitialToDo: ToDoModel = {
  id: "",
  title: "",
  description: "",
  dueDate: moment().format(DATE_FORMAT.dashWithYearFirst),
  priority: PIORYTY_OPTIONS[1].value,
};
