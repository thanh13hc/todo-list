export type CommonOption = {
  label: string;
  value: any;
};

export type ButtonColor = "primary" | "success" | "danger" | "info";

export type FormMode = "create" | "update";

export type ToDoModel = {
  id: string;
  title: string;
  description: string;
  dueDate: string;
  priority: string;
};
