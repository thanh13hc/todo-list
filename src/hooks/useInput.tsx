import { useState } from "react";

type Validate = (value: any) => string;

export const useInput = <T,>({
  validate,
  initialValue,
}: {
  validate: Validate[];
  initialValue: T;
}) => {
  const [value, setValue] = useState<T>(initialValue);
  const [isTouched, setIsTouched] = useState(false);

  const errors = validate.map((func) => func(value)).filter(Boolean);

  const hasError = errors.length !== 0 && isTouched;

  const onChange = (currenVal: T) => setValue(currenVal);

  const onBlur = (status: boolean) => setIsTouched(status);

  return { value, errors, hasError, onChange, onBlur };
};
